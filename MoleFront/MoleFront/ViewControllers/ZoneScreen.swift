//
//  ZoneScreen.swift
//  MoleFront
//
//  Created by mac on 03/02/2019.
//  Copyright © 2019 Filipp. All rights reserved.
//

import UIKit

class ZoneScreen: UIViewController {
    
    @IBOutlet weak var FemaleOff: UIButton!
    @IBOutlet weak var FemaleOn: UIButton!
    @IBOutlet weak var MaleOn: UIButton!
    @IBOutlet weak var MaleOff: UIButton!
    @IBOutlet weak var ZoneImage: UIImageView!
    
    enum maleEnum: String {
        case male = "male"
        case female = "female"
    }
    
    enum zoneEnum: String {
        case none = "none"
        case face = "face"
        case head = "head"
        case neck = "neck"
        case chest = "chest"
        case body = "body"
        case butt = "butt"
        case back = "back"
        case armLeft = "armLeft"
        case armRight = "armRight"
        case foreArmLeft = "foreArmLeft"
        case foreArmRight = "foreArmRight"
        case handLeft = "handLeft"
        case handRight = "handRight"
        case hipLeft = "hipLeft"
        case hipRight = "hipRight"
        case shinLeft = "shinLeft"
        case shinRight = "shinRight"
        case footLeft = "footLeft"
        case footRight = "footRight"
    }
    
    enum sideEnum: String {
        case front = "front"
        case back = "back"
    }
    
    var selectedMale = maleEnum.male
    var selectedZone = zoneEnum.none
    var selectedSide = sideEnum.front
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateButtonsView()
        updateZoneImage()
    }
    @IBAction func malePressed(_ sender: Any) {
        setMale(toValue: .male)
    }
    
    @IBAction func femalePressed(_ sender: Any) {
        setMale(toValue: .female)
    }
    
    
    @IBAction func revercePressed(_ sender: Any) {
        if (selectedSide == .back) {
            selectedSide = .front
        } else {
            selectedSide = .back
        }
        selectedZone = .none
        updateZoneImage()
    }
    
    func updateButtonsView() {
        MaleOn.alpha = selectedMale == maleEnum.male ? 1.0 : 0.0
        MaleOff.alpha = selectedMale == maleEnum.male ? 0.0 : 1.0
        FemaleOn.alpha = selectedMale == maleEnum.male ? 0.0 : 1.0
        FemaleOff.alpha = selectedMale == maleEnum.male ? 1.0 : 0.0
    }
    
    func setMale(toValue value: maleEnum) {
        if (value != selectedMale) {
            selectedMale = value
            selectedZone = zoneEnum.none
            updateButtonsView()
            updateZoneImage()
        }
    }
    
    func updateZoneImage() {
        switch selectedMale {
        case maleEnum.male:
            ZoneImage.image = getImageForMale()
        case maleEnum.female:
            ZoneImage.image = getImageForFemale()
        }
    }
    
    func getImageForMale() -> UIImage {
        switch selectedSide {
        case sideEnum.front:
            return getImageForMaleFront()
        case sideEnum.back:
            return getImageForMaleBack()
        }
    }
    
    func getImageForFemale() -> UIImage {
        switch selectedSide {
        case sideEnum.front:
            return getImageForFemaleFront()
        case sideEnum.back:
            return getImageForFemaleBack()
        }
    }
    
    func getImageForMaleFront() -> UIImage {
        switch selectedZone {
        case .body:
            return UIImage(named: "MF-body")!
        case .chest:
            return UIImage(named: "MF-chest")!
        case .face:
            return UIImage(named: "MF-face")!
        case .footLeft:
            return UIImage(named: "MF-foot-left")!
        case .footRight:
            return UIImage(named: "MF-foot-right")!
        case .hipLeft:
            return UIImage(named: "MF-hip-left")!
        case .hipRight:
            return UIImage(named: "MF-hip-right")!
        case .armLeft:
            return UIImage(named: "MF-left-arm")!
        case .foreArmLeft:
            return UIImage(named: "MF-left-forearm")!
        case .handLeft:
            return UIImage(named: "MF-left-hand")!
        case .neck:
            return UIImage(named: "MF-neck")!
        case .armRight:
            return UIImage(named: "MF-right-arm")!
        case .foreArmRight:
            return UIImage(named: "MF-right-forearm")!
        case .handRight:
            return UIImage(named: "MF-right-hand")!
        case .shinLeft:
            return UIImage(named: "MF-shin-left")!
        case .shinRight:
            return UIImage(named: "MF-shin-right")!
        default:
            return UIImage(named: "MF")!
        }
    }
    
    func getImageForMaleBack() -> UIImage {
        switch selectedZone {
        case .armLeft:
            return UIImage(named: "MB-arm-left")!
        case .armRight:
            return UIImage(named: "MB-arm-right")!
        case .back:
            return UIImage(named: "MB-back")!
        case .butt:
            return UIImage(named: "MB-butt")!
        case .footLeft:
            return UIImage(named: "MB-foot-left")!
        case .footRight:
            return UIImage(named: "MB-foot-right")!
        case .foreArmLeft:
            return UIImage(named: "MB-forearm-left")!
        case .foreArmRight:
            return UIImage(named: "MB-forearm-right")!
        case .handLeft:
            return UIImage(named: "MB-hand-left")!
        case .handRight:
            return UIImage(named: "MB-hand-right")!
        case .head:
            return UIImage(named: "MB-head")!
        case .hipLeft:
            return UIImage(named: "MB-hip-left")!
        case .hipRight:
            return UIImage(named: "MB-hip-right")!
        case .neck:
            return UIImage(named: "MB-neck")!
        case .shinRight:
            return UIImage(named: "MB-shin-right")!
        case .shinLeft:
            return UIImage(named: "MB-shin-left")!
        default:
            return UIImage(named: "MB")!
        }
    }
    
    func getImageForFemaleFront() -> UIImage {
        switch selectedZone {
        case .body:
            return UIImage(named: "FF-body")!
        case .chest:
            return UIImage(named: "FF-chest")!
        case .face:
            return UIImage(named: "FF-face")!
        case .footLeft:
            return UIImage(named: "FF-foot-left")!
        case .footRight:
            return UIImage(named: "FF-foot-right")!
        case .hipLeft:
            return UIImage(named: "FF-hip-left")!
        case .hipRight:
            return UIImage(named: "FF-hip-right")!
        case .armLeft:
            return UIImage(named: "FF-arm-left")!
        case .foreArmLeft:
            return UIImage(named: "FF-forearm-left")!
        case .handLeft:
            return UIImage(named: "FF-hand-left")!
        case .neck:
            return UIImage(named: "FF-neck")!
        case .armRight:
            return UIImage(named: "FF-arm-right")!
        case .foreArmRight:
            return UIImage(named: "FF-forearm-right")!
        case .handRight:
            return UIImage(named: "FF-hand-right")!
        case .shinLeft:
            return UIImage(named: "FF-shin-left")!
        case .shinRight:
            return UIImage(named: "FF-shin-right")!
        default:
            return UIImage(named: "FF")!
        }
    }
    
    func getImageForFemaleBack() -> UIImage {
        switch selectedZone {
        case .armLeft:
            return UIImage(named: "FB-arm-left")!
        case .armRight:
            return UIImage(named: "FB-arm-right")!
        case .back:
            return UIImage(named: "FB-back")!
        case .butt:
            return UIImage(named: "FB-butt")!
        case .footLeft:
            return UIImage(named: "FB-foot-left")!
        case .footRight:
            return UIImage(named: "FB-foot-right")!
        case .foreArmLeft:
            return UIImage(named: "FB-forearm-left")!
        case .foreArmRight:
            return UIImage(named: "FB-forearm-right")!
        case .handLeft:
            return UIImage(named: "FB-hand-left")!
        case .handRight:
            return UIImage(named: "FB-hand-right")!
        case .head:
            return UIImage(named: "FB-head")!
        case .hipLeft:
            return UIImage(named: "FB-hip-left")!
        case .hipRight:
            return UIImage(named: "FB-hip-right")!
        case .neck:
            return UIImage(named: "FB-neck")!
        case .shinRight:
            return UIImage(named: "FB-shin-right")!
        case .shinLeft:
            return UIImage(named: "FB-shin-left")!
        default:
            return UIImage(named: "FB")!
        }
    }
    
    @IBAction func leftHandPressed(_ sender: Any) {
        selectedZone = .handLeft
        updateZoneImage()
    }
    
    @IBAction func rightHandPressed(_ sender: Any) {
        selectedZone = .handRight
        updateZoneImage()
    }
    
    @IBAction func facePressed(_ sender: Any) {
        if (selectedSide == .back) {
            selectedZone = .head
        } else {
            selectedZone = .face
        }
        updateZoneImage()
    }
    
    @IBAction func neckPressed(_ sender: Any) {
        selectedZone = .neck
        updateZoneImage()
    }
    
    @IBAction func chestPressed(_ sender: Any) {
        if(selectedSide == .front) {
            selectedZone = .chest
        } else {
            selectedZone = .back
        }
        updateZoneImage()
    }
    
    @IBAction func bodyPressed(_ sender: Any) {
        if(selectedSide == .front) {
            selectedZone = .body
        } else {
            selectedZone = .back
        }
        updateZoneImage()
    }
    
    @IBAction func buttPressed(_ sender: Any) {
        if(selectedSide == .front) {
            selectedZone = .body
        } else {
            selectedZone = .butt
        }
        updateZoneImage()
    }
    @IBAction func leftHipPressed(_ sender: Any) {
        selectedZone = .hipLeft
        updateZoneImage()
    }
    
    @IBAction func rightHipPressed(_ sender: Any) {
        selectedZone = .hipRight
        updateZoneImage()
    }
    
    @IBAction func rightShinPressed(_ sender: Any) {
        selectedZone = .shinRight
        updateZoneImage()
    }
    
    @IBAction func leftShinPressed(_ sender: Any) {
        selectedZone = .shinLeft
        updateZoneImage()
    }
    
    @IBAction func leftFootPressed(_ sender: Any) {
        selectedZone = .footLeft
        updateZoneImage()
    }
    
    @IBAction func rightFootPressed(_ sender: Any) {
        selectedZone = .footRight
        updateZoneImage()
    }
    
    @IBAction func rightForeArmPressed(_ sender: Any) {
        selectedZone = .foreArmRight
        updateZoneImage()
    }
    
    @IBAction func rightArmPressed(_ sender: Any) {
        selectedZone = .armRight
        updateZoneImage()
    }
    
    @IBAction func leftArmPressed(_ sender: Any) {
        selectedZone = .armLeft
        updateZoneImage()
    }
    
    @IBAction func leftForeArmPressed(_ sender: Any) {
        selectedZone = .foreArmLeft
        updateZoneImage()
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String?, sender: Any?) -> Bool {
        if let ident = identifier {
            if ident == "fromZoneToCamera" {
                if selectedZone == .none {
                    let alert = UIAlertController(title: "Ошибка", message: "Выберите зону", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default))
                    self.present(alert, animated: true, completion: nil)
                    return false
                }
            }
        }
        return true
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? CameraScreen {
            vc.zone = selectedZone
            vc.male = selectedMale
            vc.side = selectedSide
        }
    }
}
