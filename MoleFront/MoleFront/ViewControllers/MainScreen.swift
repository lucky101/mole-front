//
//  MainScreen.swift
//  MoleFront
//
//  Created by mac on 12/03/2019.
//  Copyright © 2019 Filipp. All rights reserved.
//

import UIKit

class MainScreen: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UserDefaults.standard.value(forKey: "token") == nil) {
            register()
        }
        
        print("token: ", UserDefaults.standard.value(forKey: "token") ?? "no token")
    }
    

    @IBAction func checkButtonPressed(_ sender: Any) {
        if UserDefaults.standard.value(forKey: "isInstructionsShowed") == nil {
            performSegue(withIdentifier: "fromMainToInstructions", sender: nil)
        } else {
            performSegue(withIdentifier: "fromMainToZone", sender: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
