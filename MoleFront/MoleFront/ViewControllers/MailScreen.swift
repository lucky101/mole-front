//
//  MailScreen.swift
//  MoleFront
//
//  Created by mac on 09/02/2019.
//  Copyright © 2019 Filipp. All rights reserved.
//

import UIKit

class MailScreen: UIViewController {

    @IBOutlet weak var mailTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mailTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: mailTextField.frame.height))
        mailTextField.leftViewMode = .always
        mailTextField.layer.cornerRadius = 8
        // Do any additional setup after loading the view.
    }
    
    @IBAction func sendPressed(_ sender: Any) {
        sendButton.setTitle("Отправлено", for: UIControl.State.normal)
        // #b2b2b2
        sendButton.backgroundColor = UIColor(red: 178.0/256.0, green: 178.0/256.0, blue: 178.0/256.0, alpha: 1)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.performSegue(withIdentifier: "fromMailToResult", sender: nil)
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
