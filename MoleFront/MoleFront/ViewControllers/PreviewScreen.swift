//
//  PreviewScreen.swift
//  MoleFront
//
//  Created by mac on 07/02/2019.
//  Copyright © 2019 Filipp. All rights reserved.
//

import UIKit

class PreviewScreen: UIViewController {
    
    var zone: ZoneScreen.zoneEnum? = nil
    var male: ZoneScreen.maleEnum? = nil
    var side: ZoneScreen.sideEnum? = nil
    
    var previewImage: UIImage? = nil
    
    @IBOutlet weak var blackDotPreview: UIImageView!
    @IBOutlet weak var redDotPreview: UIImageView!
    @IBOutlet weak var centerDotPreview: UIImageView!
    @IBOutlet weak var greenDotPreview: UIImageView!
    @IBOutlet weak var blueDotPreview: UIImageView!
    
    @IBOutlet weak var maskImage: UIImageView!
    @IBOutlet weak var realPreviewImageView: UIImageView!
    
    @IBOutlet weak var previewImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(getIphoneModel() == "iPhone X") {
            maskImage.image = UIImage(named: "PS-mask-x")
        }
        
        realPreviewImageView.image = previewImage
        
        previewImageView.image = cropMole()
        centerDotPreview.image = cropCenterDot()
        greenDotPreview.image = cropGreenDot()
        redDotPreview.image = cropRedDot()
        blueDotPreview.image = cropBlueDot()
        blackDotPreview.image = cropBlackDot()
    }
    
    func getIphoneModel() -> String {
            var systemInfo = utsname()
            uname(&systemInfo)
            let machineMirror = Mirror(reflecting: systemInfo.machine)
            let identifier = machineMirror.children.reduce("") { identifier, element in
                guard let value = element.value as? Int8, value != 0 else { return identifier }
                return identifier + String(UnicodeScalar(UInt8(value)))
            }
            
            switch identifier {
            case "iPod5,1":                                 return "iPod Touch 5"
            case "iPod7,1":                                 return "iPod Touch 6"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone X"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone X"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
            case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
            case "AppleTV5,3":                              return "Apple TV"
            case "i386", "x86_64":                          return "Simulator"
            default:                                        return identifier
            }
    
    }
    
    func getConstats() -> Int {
        let model = getIphoneModel()
        print(model)
        switch model {
        case "iPhone X":
            return 125
        default:
            return 100
        }
    }
    
    func additionalConstantForX() -> Int {
        if (getIphoneModel() == "iPhone X"){
            return -70
        } else {
            return 0
        }
    }
    
    func getSize() -> Int {
        let model = getIphoneModel()
        print(model)
        switch model {
        case "iPhone X":
            return 160
        default:
            return 125
        }
    }
    
    func cropMole() -> UIImage {
        let imageCopy = previewImage?.copy() as! UIImage
        let unwrappedPreview = previewImage!
        let cgImageUnwrappedPreview = unwrappedPreview.cgImage!
        
        let a = getConstats()
        
        let width = 400
        let height = 400
        let x = cgImageUnwrappedPreview.width/2 - width - additionalConstantForX()
        let y = cgImageUnwrappedPreview.height/2 - height/2
        let cropArea = CGRect(x: x, y: y, width: width, height: height)
        
        let imgOrientation = unwrappedPreview.imageOrientation
        let imgScale = unwrappedPreview.scale
        let croppedCGImage = cgImageUnwrappedPreview.cropping(to: cropArea)
        let coreImage = CIImage(cgImage: croppedCGImage!)
        let ciContext = CIContext(options: nil)
        let filteredImageRef = ciContext.createCGImage(coreImage, from: coreImage.extent)
        let finalImage = UIImage(cgImage:filteredImageRef!, scale:imgScale, orientation:imgOrientation)
        
        return finalImage
    }
    
    func cropCenterDot() -> UIImage {
        let imageCopy = previewImage?.copy() as! UIImage
        let unwrappedPreview = previewImage!
        let cgImageUnwrappedPreview = unwrappedPreview.cgImage!
        
        let width = getSize()
        let height = getSize()
        let x = cgImageUnwrappedPreview.width/2 - width/2 + 250 - additionalConstantForX()
        let y = cgImageUnwrappedPreview.height/2 - height/2
        let cropArea = CGRect(x: x, y: y, width: width, height: height)
        
        let imgOrientation = unwrappedPreview.imageOrientation
        let imgScale = unwrappedPreview.scale
        let croppedCGImage = cgImageUnwrappedPreview.cropping(to: cropArea)
        let coreImage = CIImage(cgImage: croppedCGImage!)
        let ciContext = CIContext(options: nil)
        let filteredImageRef = ciContext.createCGImage(coreImage, from: coreImage.extent)
        let finalImage = UIImage(cgImage:filteredImageRef!, scale:imgScale, orientation:imgOrientation)
        
        return finalImage
    }
    
    func cropRedDot() -> UIImage {
        let imageCopy = previewImage?.copy() as! UIImage
        let unwrappedPreview = previewImage!
        let cgImageUnwrappedPreview = unwrappedPreview.cgImage!
        
        let width = getSize()
        let height = getSize()
        let x = cgImageUnwrappedPreview.width/2 - width/2 + 250 - getConstats() - additionalConstantForX()
        let y = cgImageUnwrappedPreview.height/2 - height/2 - getConstats()
        let cropArea = CGRect(x: x, y: y, width: width, height: height)
        
        let imgOrientation = unwrappedPreview.imageOrientation
        let imgScale = unwrappedPreview.scale
        let croppedCGImage = cgImageUnwrappedPreview.cropping(to: cropArea)
        let coreImage = CIImage(cgImage: croppedCGImage!)
        let ciContext = CIContext(options: nil)
        let filteredImageRef = ciContext.createCGImage(coreImage, from: coreImage.extent)
        let finalImage = UIImage(cgImage:filteredImageRef!, scale:imgScale, orientation:imgOrientation)
        
        return finalImage
    }
    
    func cropBlackDot() -> UIImage {
        let imageCopy = previewImage?.copy() as! UIImage
        let unwrappedPreview = previewImage!
        let cgImageUnwrappedPreview = unwrappedPreview.cgImage!
        
        let width = getSize()
        let height = getSize()
        let x = cgImageUnwrappedPreview.width/2 - width/2 + 250 - getConstats() - additionalConstantForX()
        let y = cgImageUnwrappedPreview.height/2 - height/2 + getConstats()
        let cropArea = CGRect(x: x, y: y, width: width, height: height)
        
        let imgOrientation = unwrappedPreview.imageOrientation
        let imgScale = unwrappedPreview.scale
        let croppedCGImage = cgImageUnwrappedPreview.cropping(to: cropArea)
        let coreImage = CIImage(cgImage: croppedCGImage!)
        let ciContext = CIContext(options: nil)
        let filteredImageRef = ciContext.createCGImage(coreImage, from: coreImage.extent)
        let finalImage = UIImage(cgImage:filteredImageRef!, scale:imgScale, orientation:imgOrientation)
        
        return finalImage
    }
    
    func cropBlueDot() -> UIImage {
        let imageCopy = previewImage?.copy() as! UIImage
        let unwrappedPreview = previewImage!
        let cgImageUnwrappedPreview = unwrappedPreview.cgImage!
        
        let width = getSize()
        let height = getSize()
        let x = cgImageUnwrappedPreview.width/2 - width/2 + 250 + getConstats() - additionalConstantForX()
        let y = cgImageUnwrappedPreview.height/2 - height/2 + getConstats()
        let cropArea = CGRect(x: x, y: y, width: width, height: height)
        
        let imgOrientation = unwrappedPreview.imageOrientation
        let imgScale = unwrappedPreview.scale
        let croppedCGImage = cgImageUnwrappedPreview.cropping(to: cropArea)
        let coreImage = CIImage(cgImage: croppedCGImage!)
        let ciContext = CIContext(options: nil)
        let filteredImageRef = ciContext.createCGImage(coreImage, from: coreImage.extent)
        let finalImage = UIImage(cgImage:filteredImageRef!, scale:imgScale, orientation:imgOrientation)
        
        return finalImage
    }
    
    func cropGreenDot() -> UIImage {
        let imageCopy = previewImage?.copy() as! UIImage
        let unwrappedPreview = previewImage!
        let cgImageUnwrappedPreview = unwrappedPreview.cgImage!
        
        let width = getSize()
        let height = getSize()
        let x = cgImageUnwrappedPreview.width/2 - width/2 + 250 + getConstats() - additionalConstantForX()
        let y = cgImageUnwrappedPreview.height/2 - height/2 - getConstats()
        let cropArea = CGRect(x: x, y: y, width: width, height: height)
        
        let imgOrientation = unwrappedPreview.imageOrientation
        let imgScale = unwrappedPreview.scale
        let croppedCGImage = cgImageUnwrappedPreview.cropping(to: cropArea)
        let coreImage = CIImage(cgImage: croppedCGImage!)
        let ciContext = CIContext(options: nil)
        let filteredImageRef = ciContext.createCGImage(coreImage, from: coreImage.extent)
        let finalImage = UIImage(cgImage:filteredImageRef!, scale:imgScale, orientation:imgOrientation)
        
        return finalImage
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let d = segue.destination as? CameraScreen{
            d.zone = zone
            d.male = male
            d.side = side
        }
        if let d = segue.destination as? ResultScreen{
            scan(moleImage: previewImageView.image!, redDot: redDotPreview.image!, greenDot: greenDotPreview.image!, blueDot: blueDotPreview.image!, blackDot: blackDotPreview.image!, centerDot: centerDotPreview.image!, zone: zone!, male: male!, side: side!, complition: {id in
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                    getResult(forId: id, completion: { res in
                        DispatchQueue.main.async {
                            d.loading = false
                            print(res["chance"] as? Double, res["space"] as? Double)
                            
                            func formDiametr(d: Any?) -> String {
                                if let d_int = d as? Int {
                                    return "\(String(d_int)) мм"
                                } else {
                                    return "0 мм"
                                }
                            }
                            
                            func formContour (b: Any?) -> String {
                                if let b_int = b as? Int {
                                    return b_int == 1 ? "Нарушена" : "Не нарушена"
                                } else {
                                    return "Не нарушена"
                                }
                            }
                            
                            d.results = [
                                Result(title: "Цвет", value: res["color"] as? String ?? "коричневый"),
                                Result(title: "Резкость границ", value: formContour(b: res["countur"])),
                                Result(title: "Размер", value: res["space"] as? String ?? ""),
                                Result(title: "Форма", value: res["form"] as? String ?? "Не определено"),
                                Result(title: "Мин. диаметр", value: formDiametr(d: res["min_diameter"])),
                                Result(title: "Макс. диаметр", value: formDiametr(d: res["max_diameter"]))
                            ]
                            d.result = Double(res["chance"] as? String ?? "") ?? 0.0
                            
                            
                            
//                            d.color = res["color"] as? String ?? "коричневый"
//                            d.mm = Double(res["space"]  as? String ?? "") ?? 0.0
//                            d.contour = Int(res["countur"]  as? String ?? "") ?? 0
                            d.previewImage = self.previewImage
                            d.shareBtn.isEnabled = true
                            
                            d.updateResults()
                        }
                        
                    })
                }
            })
        }
    }
}
