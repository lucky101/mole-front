//
//  InstuctionScreen.swift
//  MoleFront
//
//  Created by mac on 28/01/2019.
//  Copyright © 2019 Filipp. All rights reserved.
//

import Foundation
import UIKit

class InstuctionScreen: UIViewController {
    @IBOutlet weak var Image1: UIImageView!
    @IBOutlet weak var Image2: UIImageView!
    @IBOutlet weak var Image3: UIImageView!
    
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var DescriptionLabel: UILabel!
    
    @IBOutlet weak var PageControl: UIPageControl!
    @IBOutlet weak var DownloadButton: UIButton!
    
    let deltaXForPhotos = CGFloat(240.0)
    let animationDuration = 0.5
    var currentPhoto = 1
    var smallHeigt = CGFloat(1.0)
    var bigHeight = CGFloat(1.0)
    var smallWidth = CGFloat(1.0)
    var bigWidth = CGFloat(1.0)
    
    override func viewDidLoad() {
        UserDefaults.standard.set(true, forKey: "isInstructionsShowed")
        
        Image2.center.x += deltaXForPhotos
        Image3.center.x += 2*deltaXForPhotos
        currentPhoto = 1
        
        let temp = CGFloat(0.8)
        
        self.Image2.transform = CGAffineTransform(scaleX: temp, y: temp)
        self.Image3.transform = CGAffineTransform(scaleX: temp, y: temp)
        
        bigHeight = Image1.frame.size.height
        smallHeigt = Image2.frame.size.height
        bigWidth = Image1.frame.size.width
        smallWidth = Image2.frame.size.width
    }
    
    @IBAction func DownloadPressed(_ sender: Any) {
        guard let url = URL(string: "https://drive.google.com/file/d/1-MU89Qn_hNenydGNy-qgjFvS11w1V2-t/view?usp=sharing") else { return }
        UIApplication.shared.open(url)
    }
    
    
    @IBAction func nextPressed(_ sender: Any) {
        if(currentPhoto == 3) {
            performSegue(withIdentifier: "fromInstructionToZone", sender: nil)
        }
        makeNext()
    }
    
    func makeNext() {
        if (currentPhoto == 3) {
            return 
        } else {
            animate(toRight: true)
            currentPhoto += 1
            PageControl.currentPage += 1
        }
    }
    
    func makePrevious() {
        if (currentPhoto == 1) {
            return
        } else {
            animate(toRight: false)
            currentPhoto -= 1
            PageControl.currentPage -= 1
        }
    }
    
    func animate(toRight: Bool) {
        let direction = toRight ? -1 : 1
        
        UIView.animate(withDuration: animationDuration, animations: {
            self.Image1.center.x += self.deltaXForPhotos * CGFloat(direction)
            self.Image2.center.x += self.deltaXForPhotos * CGFloat(direction)
            self.Image3.center.x += self.deltaXForPhotos * CGFloat(direction)

            self.Image1.transform = CGAffineTransform(scaleX: self.getScaleMultiplicator(num: 1, toRight: toRight), y:self.getScaleMultiplicator(num: 1, toRight: toRight))
            
            self.Image2.transform = CGAffineTransform(scaleX: self.getScaleMultiplicator(num: 2, toRight: toRight), y:self.getScaleMultiplicator(num: 2, toRight: toRight))
            
            self.Image3.transform = CGAffineTransform(scaleX: self.getScaleMultiplicator(num: 3, toRight: toRight), y:self.getScaleMultiplicator(num: 3, toRight: toRight))
        })
        
        UIView.animate(withDuration: animationDuration/2, animations: {
            self.TitleLabel.alpha = 0.0
            self.DescriptionLabel.alpha = 0.0
            if (self.currentPhoto == 1) {
                self.DownloadButton.alpha = 0.0
            }
        })
        
        setText(forNum: currentPhoto - direction)
        
        UIView.animate(withDuration: animationDuration/2, delay: animationDuration/2, animations: {
            self.TitleLabel.alpha = 1.0
            self.DescriptionLabel.alpha = 1.0
            if (self.currentPhoto == 2 && direction == 1){
                self.DownloadButton.alpha = 1.0
            }
        })
    }
    
    func setText(forNum num: Int) {
        print(num)
        switch num {
        case 1:
            TitleLabel.text = "Подготовьте шаблон"
            DescriptionLabel.text = "Скачайте шаблон и распечатайте на цветном принтере"
            
        case 2:
            TitleLabel.text = "Вырежьте шаблон"
            DescriptionLabel.text = "Вырежьте распечатанный шаблон по контуру"

        case 3:
            TitleLabel.text = "Сфотографируйте"
            DescriptionLabel.text = "Положите шаблон около родинки, совместите реальный шаблон с шаблоном на экране"
        default:
            TitleLabel.text = "Подготовьте шаблон"
            DescriptionLabel.text = "Скачайте шаблон и распечатайте на цветном принтере"
        }
    }
    
    @IBAction func swipeHandler(_ sender: UISwipeGestureRecognizer) {
        if sender.state == .ended {
            if sender.direction == .left {
                makeNext()
            }
            if sender.direction == .right {
                makePrevious()
            }
        }
    }
    
    func getScaleMultiplicator(num: Int, toRight: Bool) -> CGFloat {
        let delta = toRight ? 1 : -1
        if (num == currentPhoto + delta) {
            return 1.0
        } else {
            return 0.8
        }
    }
    
    func getDeltaHeight(num: Int, toRight: Bool) -> CGFloat {
        if (num == currentPhoto) {
            return 0.1*bigHeight
        }
        if (toRight) {
            if (num == currentPhoto + 1) {
                return -0.1 * bigHeight
            }
        } else {
            if (num == currentPhoto - 1) {
                return -0.1 * bigHeight
            }
        }
        return 1.0
    }
    
    func getDeltaWidth(num: Int, toRight: Bool) -> CGFloat {
        if (num == currentPhoto) {
            return -0.1*bigWidth
        }
        if (toRight) {
            if (num == currentPhoto + 1) {
                return 0.1 * bigWidth
            }
        } else {
            if (num == currentPhoto - 1) {
                return 0.1 * bigWidth
            }
        }
        return 1.0
    }
}
