//
//  ResultScreen.swift
//  MoleFront
//
//  Created by mac on 12/03/2019.
//  Copyright © 2019 Filipp. All rights reserved.
//

import UIKit

class ResultScreen: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    var previewImage: UIImage? = nil
    
    var loading = true
    var result = 0.0
    var contour = 0
    var color = "светло-коричневый"
    var mm = 0.0
    var dataOpacity = 0.0
    
    var results = [
        Result(title: "Цвет", value: ""),
        Result(title: "Резкость границ", value: ""),
        Result(title: "Размер", value: ""),
        Result(title: "Форма", value: ""),
        Result(title: "Мин. диаметр", value: ""),
        Result(title: "Макс. диаметр", value: "")
    ]


    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    let cellReuseIdentifier = "cell"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        self.tableView.register(MFTableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell:MFTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! MFTableViewCell
        
        // set the text from the data model
        cell.TitleLabel?.text = "\(self.results[indexPath.row].title)"
        cell.ValueLabel?.text = "\(self.results[indexPath.row].value)"
        cell.ValueLabel?.alpha = CGFloat(dataOpacity)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.results.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88.5;
    }

    @IBOutlet weak var shareBtn: UIButton!
    
    @IBAction func shareButton(_ sender: Any) {
//        UIGraphicsBeginImageContext(view.frame.size)
//        view.layer.render(in: UIGraphicsGetCurrentContext()!)
//        let image = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
        
        let IntResult = round(result)
        let IntMM = round(mm)
        
        let textToShare = "Сканер родинок, результат обработки:\nШанс: \(results[0].value)\nРезкость границ \(results[1].value)\nРазмер \(results[2].value)\nФорма \(results[3].value)\nМинимальный диаметр \(results[4].value)\nМаксимальный диаметр \(results[5].value)\n"
        
        let objectsToShare = [textToShare, previewImage!.cgImage!] as [Any]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

        //Excluded Activities
        activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
        //
        
        activityVC.popoverPresentationController?.sourceView = sender as! UIView
        self.present(activityVC, animated: true, completion: nil)

        
//        if let myWebsite = URL(string: "http://itunes.apple.com/app/idXXXXXXXXX") {//Enter link to your app here
////            let objectsToShare = [textToShare, image ?? #imageLiteral(resourceName: "app-logo")] as [Any]
////            let objectsToShare = [textToShare] as [Any]
////            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
////
////            //Excluded Activities
////            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
////            //
////
////            activityVC.popoverPresentationController?.sourceView = sender as! UIView
////            self.present(activityVC, animated: true, completion: nil)
//        }
    }
    
    func updateResults() {
        let IntResult = round(result)
        resultLabel.text = "\(IntResult)%"
        tableView.reloadData()
        dataOpacity = 1.0
        
        activityIndicator.alpha = 0.0
        resultLabel.alpha = 1.0

        
        
//        let results = []
        
        
        
//        colorLabel.text = color
//        if (contour == 1) {
//            contourLabel.text = "Нарушен"
//        } else {
//            contourLabel.text = "Не нарушен"
//        }
//
//        let IntMM = round(mm)
//        sizeLabel.text = "\(IntMM)"
//
//        print(result, mm, contour, color)
//        sizeLabel.alpha = 1.0
//        colorLabel.alpha = 1.0
//        contourLabel.alpha = 1.0
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    

}
