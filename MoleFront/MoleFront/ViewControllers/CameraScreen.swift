//
//  CameraScreen.swift
//  MoleFront
//
//  Created by mac on 07/02/2019.
//  Copyright © 2019 Filipp. All rights reserved.
//

import UIKit
import AVFoundation

class CameraScreen: UIViewController, AVCapturePhotoCaptureDelegate {

    var captureSession: AVCaptureSession!
    var stillImageOutput: AVCapturePhotoOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    var takenPhoto: UIImage?
    var selectedCamera = AVCaptureDevice.Position.back
    
    var zone: ZoneScreen.zoneEnum? = nil
    var male: ZoneScreen.maleEnum? = nil
    var side: ZoneScreen.sideEnum? = nil
    
    
    @IBOutlet weak var previewView: UIView!
    
    @IBAction func downloadPressed(_ sender: Any) {
        guard let url = URL(string: "https://drive.google.com/file/d/1-MU89Qn_hNenydGNy-qgjFvS11w1V2-t/view?usp=sharing") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func didTakePhoto(_ sender: Any) {
        let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        stillImageOutput.capturePhoto(with: settings, delegate: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    func getDevice(position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        let devices: NSArray = AVCaptureDevice.devices() as NSArray;
        for de in devices {
            let deviceConverted = de as! AVCaptureDevice
            if(deviceConverted.position == position){
                return deviceConverted
            }
        }
        return nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        configureCamera()
    }
    
    func configureCamera() {
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = .high
        
        guard let camera = getDevice(position: selectedCamera) else {
            return
        }
        
        do {
            let input = try AVCaptureDeviceInput(device: camera)
            stillImageOutput = AVCapturePhotoOutput()
            
            if captureSession.canAddInput(input) && captureSession.canAddOutput(stillImageOutput) {
                captureSession.addInput(input)
                captureSession.addOutput(stillImageOutput)
                setupLivePreview()
            }
            
            try! camera.lockForConfiguration()
            camera.focusMode = .continuousAutoFocus
            camera.unlockForConfiguration()
        }
        catch let error  {
            print("Error Unable to initialize back camera:  \(error.localizedDescription)")
        }
    }
    
    func setupLivePreview() {
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        
        videoPreviewLayer.videoGravity = .resizeAspect
        videoPreviewLayer.connection?.videoOrientation = .portrait
        previewView.layer.addSublayer(videoPreviewLayer)
        
        DispatchQueue.global(qos: .userInitiated).async { //[weak self] in
            self.captureSession.startRunning()
            
            DispatchQueue.main.async {
                self.videoPreviewLayer.frame = self.previewView.bounds
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.captureSession.stopRunning()
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        guard let imageData = photo.fileDataRepresentation()
            else { return }
        
        takenPhoto = UIImage(data: imageData)
        
        performSegue(withIdentifier: "fromCameraToPreview", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "fromCameraToPreview") {
            if let d = segue.destination as? PreviewScreen{
                d.previewImage = takenPhoto
                d.zone = zone
                d.male = male
                d.side = side
            }
        }
    }
    
    func toggleFlash() {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { return }
        
        do {
            try device.lockForConfiguration()
            
            if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                device.torchMode = AVCaptureDevice.TorchMode.off
            } else {
                do {
                    try device.setTorchModeOn(level: 1.0)
                } catch {
                    print(error)
                }
            }
            
            device.unlockForConfiguration()
        } catch {
            print(error)
        }
    }
    
    @IBAction func flashPressed(_ sender: Any) {
        toggleFlash()
    }
    
    
    @IBAction func toggleCameraPressed(_ sender: Any) {
        if(selectedCamera == .front) {
            selectedCamera = .back
        } else {
            selectedCamera = .front
        }
        
        configureCamera()
    }
    
    
}
