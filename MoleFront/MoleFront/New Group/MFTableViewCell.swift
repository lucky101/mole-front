//
//  MFTableViewCell.swift
//  MoleFront
//
//  Created by mac on 05/07/2019.
//  Copyright © 2019 Filipp. All rights reserved.
//

import UIKit

class MFTableViewCell: UITableViewCell {
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var ValueLabel: UILabel!
}
