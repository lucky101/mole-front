//
//  Result.swift
//  MoleFront
//
//  Created by mac on 05/07/2019.
//  Copyright © 2019 Filipp. All rights reserved.
//

import Foundation

struct Result {
    
    var title:String
    var value:String
    
    // Constructor.
    init(title:String, value:String) {
        self.title = title;
        self.value = value;
    }
    
}
