//
//  Actions.swift
//  MoleFront
//
//  Created by mac on 11/03/2019.
//  Copyright © 2019 Filipp. All rights reserved.
//

import Foundation
import UIKit

let defaults = UserDefaults.standard

let BASE_URL = "http://185.246.65.1/"

func register() {
    let uuid = UIDevice.current.identifierForVendor?.uuidString
    let parameters = ["uuid": uuid! + "a"]
    
    let additionalString =  "secret_token/register/"
    let url = URL(string: BASE_URL + additionalString)
    
    let session = URLSession.shared
    
    var request = URLRequest(url: url!)
    request.httpMethod = "POST"
    
    do {
        request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
    } catch let error {
        print(error.localizedDescription)
    }
    
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    
    let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
        
        guard error == nil else {
            return
        }
        
        guard let data = data else {
            return
        }
        
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                print("Response \(json)")
                
                defaults.set(json["token"], forKey: "token")
            }
        } catch let error {
            print(error.localizedDescription)
        }
    })
    task.resume()
}

func scan(moleImage: UIImage, redDot: UIImage, greenDot: UIImage, blueDot: UIImage, blackDot: UIImage, centerDot: UIImage, zone: ZoneScreen.zoneEnum, male: ZoneScreen.maleEnum, side: ZoneScreen.sideEnum, complition: @escaping (Int) -> Void) {
    let token = defaults.value(forKey: "token") as! String
    
    let moleImageData = moleImage.jpegData(compressionQuality: 1.0)!
    let redImageData = redDot.jpegData(compressionQuality: 1.0)!
    let greenImageData = greenDot.jpegData(compressionQuality: 1.0)!
    let blueImageData = blueDot.jpegData(compressionQuality: 1.0)!
    let blackImageData = blackDot.jpegData(compressionQuality: 1.0)!
    let centerImageData = centerDot.jpegData(compressionQuality: 1.0)!
    
    
    let requestURL = URL(string: BASE_URL+token+"/scan/")!
    let session = URLSession(configuration: URLSessionConfiguration.default)
    var request:URLRequest = URLRequest(url: requestURL)
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    request.httpMethod = "POST"
    
    let boundary = "Boundary-\(NSUUID().uuidString)"
    request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
    
//    let parameters = ["photo": imageData, "zone": zone, "side": side, "gender": male] as [String : Any]
    // mole, center_dot, red_dot, blue_dot, green_dot, black_dot
    let parameters = ["mole": moleImageData, "center_dot": centerImageData, "red_dot": redImageData, "blue_dot": blueImageData, "green_dot": greenImageData, "black_dot": blackImageData]
    
    do {
        let body = NSMutableData()
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format:"Content-Disposition: form-data; name=\"mole_img\"; filename=\"mole_filename.jpg\"\r\n").data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format: "Content-Type: application/octet-stream\r\n\r\n").data(using: String.Encoding.utf8.rawValue)!)
        body.append(moleImageData)
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format:"Content-Disposition: form-data; name=\"red_dot\"; filename=\"calib_red_filename.jpg\"\r\n").data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format: "Content-Type: application/octet-stream\r\n\r\n").data(using: String.Encoding.utf8.rawValue)!)
        body.append(redImageData)
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format:"Content-Disposition: form-data; name=\"green_dot\"; filename=\"calib_green_filename.jpg\"\r\n").data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format: "Content-Type: application/octet-stream\r\n\r\n").data(using: String.Encoding.utf8.rawValue)!)
        body.append(greenImageData)
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format:"Content-Disposition: form-data; name=\"blue_dot\"; filename=\"calib_blue_filename.jpg\"\r\n").data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format: "Content-Type: application/octet-stream\r\n\r\n").data(using: String.Encoding.utf8.rawValue)!)
        body.append(blueImageData)
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format:"Content-Disposition: form-data; name=\"black_dot\"; filename=\"calib_black_filename.jpg\"\r\n").data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format: "Content-Type: application/octet-stream\r\n\r\n").data(using: String.Encoding.utf8.rawValue)!)
        body.append(blackImageData)
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format:"Content-Disposition: form-data; name=\"center_dot\"; filename=\"calib_gray_filename.jpg\"\r\n").data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format: "Content-Type: application/octet-stream\r\n\r\n").data(using: String.Encoding.utf8.rawValue)!)
        body.append(centerImageData)
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format: "Content-Disposition: form-data; name=\"zone\"\r\n\r\n" as NSString).data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format: zone.rawValue as NSString).data(using: String.Encoding.utf8.rawValue)!)
        
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format: "Content-Disposition: form-data; name=\"gender\"\r\n\r\n" as NSString).data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format: male.rawValue as NSString).data(using: String.Encoding.utf8.rawValue)!)
        
        body.append(NSString(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format: "Content-Disposition: form-data; name=\"side\"\r\n\r\n" as NSString).data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format: side.rawValue as NSString).data(using: String.Encoding.utf8.rawValue)!)
        
        request.httpBody = body as Data
    } catch let error {
        print(error.localizedDescription)
    }
    
    let task = session.dataTask(with: request) { (data, response, error) in
        guard error == nil else {
            print("error in scan")
            return
        }
        
        guard let data = data else {
            print("no data in scan")
            return
        }
        
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                print("Response \(json)")
                if (json["detail"] != nil) {
                    return
                }
                let id = json["id"] as! Int
                complition(id)
            }
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    
    
    print("request", requestURL, request.httpBody)
    
    task.resume()
}

func getResult(forId id: Int, completion: @escaping ([String: Any]) -> Void){
    let token = defaults.value(forKey: "token") as! String
    
    
    
    let additionalString =  "\(token)/result/\(id)/"
    let url = URL(string: BASE_URL + additionalString)
    
    let session = URLSession.shared
    
    var request = URLRequest(url: url!)
    request.httpMethod = "GET"
    
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    
    let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
        guard error == nil else {
            return
        }
        
        guard let data = data else {
            return
        }
        
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                print("Response \(json)")
                completion(json)
            }
        } catch let error {
            print(error.localizedDescription)
        }
    })
    
    print("request", url)
    
    task.resume()
}
